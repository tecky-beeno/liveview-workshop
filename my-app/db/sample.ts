import { proxy } from './proxy'

// This file serve like the knex seed file.
//
// You can setup the database with sample data via the db proxy.

// console.log(
//   'received methods:',
//   proxy.method.map(row => row.method),
// )

proxy.todo_item[1] = {
  title: 'Buy apple',
}
proxy.todo_item[2] = {
  title: 'Buy milk',
}
proxy.todo_item[3] = {
  title: 'Learn ts-liveview',
}
