import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {

  if (!(await knex.schema.hasTable('todo_item'))) {
    await knex.schema.createTable('todo_item', table => {
      table.increments('id')
      table.text('title').notNullable()
      table.timestamps(false, true)
    })
  }
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('todo_item')
}
