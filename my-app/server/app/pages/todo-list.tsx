import { proxy } from '../../../db/proxy.js'
import { mapArray } from '../components/fragment.js'
import Style from '../components/style.js'
import { Context } from '../context.js'
import { o } from '../jsx/jsx.js'
import { getContextFormBody } from '../context.js'
import { object, string } from 'cast.ts'

let TodoList = (
  <div id="todoList">
    {Style(/* css */ `
#todoList .items {
	border: 1px solid #ccc;
}
#todoList .item {
	border: 1px solid #eee;
	padding: 1rem;
}
`)}
    <h2>Todo List</h2>
    <form method="POST" action="/todo-list/item" onsubmit="emitForm(event)">
      <input type="text" name="title" />
      <input type="submit" />
    </form>
    <div className="items">
      <div className="item">Item 1</div>
      <div className="item">Item 2</div>
      <div className="item">Item 3</div>
      <Items />
    </div>
  </div>
)

function Items() {
  return (
    <>
      {mapArray(proxy.todo_item, item => (
        <div class="item">{item.title}</div>
      ))}
    </>
  )
}

let addItemParser = object({
  title: string({ nonEmpty: true, trim: true }),
})
function addItem(attrs: {}, context: Context) {
  let body = addItemParser.parse(getContextFormBody(context))
  console.log('body:', body)
  proxy.todo_item.push({ title: body.title })
  return TodoList
}

export default {
  index: TodoList,
  addItem,
}
